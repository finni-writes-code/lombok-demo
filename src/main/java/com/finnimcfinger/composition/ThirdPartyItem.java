package com.finnimcfinger.composition;

import lombok.Data;
import lombok.experimental.Delegate;

@Data
public class ThirdPartyItem {
	private int itemId;
	
	@Delegate(types= {Contact.class})
	private final Contact support = new SupportContact();
}
