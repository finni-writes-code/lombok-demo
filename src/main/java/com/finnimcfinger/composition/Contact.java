package com.finnimcfinger.composition;

public interface Contact {
	String getFirstName();
	void setFirstName(String firstName);
	String getLastName();
	void setLastName(String lastName);
	String getFullName();
	String getPhoneNumber();
	void setPhoneNumber(String phoneNumber);
}
