package com.finnimcfinger.composition;

public class ThirdPartyContactTest {
	public static void main(String[] args) {
		ThirdPartyItem widget = new ThirdPartyItem();
		
		widget.setItemId(1);
		widget.setFirstName("Finni");
		widget.setLastName("McFinger");
		widget.setPhoneNumber("555-985-2398");
		
		System.out.println(widget.getFullName());
	}
}
