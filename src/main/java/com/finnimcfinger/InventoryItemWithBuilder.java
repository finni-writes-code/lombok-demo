package com.finnimcfinger;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Builder(builderClassName="Builder", toBuilder=true)
public class InventoryItemWithBuilder {
	final private int id;
	final private int qtyOnHand;
	final private int priceInCents;
	final private int restockTimeInDays;
	final private int bulkDiscountMinimum;
	final private double bulkDiscount;
	final private String name; 
	final private String shortDescription; 
	final private String longDescription;
}
