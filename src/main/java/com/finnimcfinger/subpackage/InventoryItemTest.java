package com.finnimcfinger.subpackage;

import com.finnimcfinger.InventoryItem;

public class InventoryItemTest {
	public static void main(String[] args) {
		InventoryItem widget = new InventoryItem();
		
		widget.setId(100);
		widget.setQtyOnHand(250);
		widget.setPriceInCents(150);
		widget.setRestockTimeInDays(4);
		widget.setBulkDiscountMinimum(25);
		widget.setBulkDiscount(0.1d);
		widget.setName("Widget");
		widget.setShortDescription("a widget");
		widget.setLongDescription("This is a standard widget.");
		
		System.out.println(widget);
		
		InventoryItem superWidget = new InventoryItem();
		
		superWidget.setId(101);
		superWidget.setQtyOnHand(250);
		superWidget.setPriceInCents(150);
		superWidget.setRestockTimeInDays(4);
		superWidget.setBulkDiscountMinimum(25);
		superWidget.setBulkDiscount(0.1d);
		superWidget.setName("Super Widget");
		superWidget.setShortDescription("a super widget");
		superWidget.setLongDescription("This is not your daddy's widget. It's super!");
		
		System.out.println(superWidget);
		System.out.println("Equality test, expected false: " + superWidget.equals(widget));
		System.out.println("Hash code test, expected false: " + (superWidget.hashCode() == widget.hashCode()));
	}
}
