package com.finnimcfinger.subpackage;

import com.finnimcfinger.InventoryItemWithBuilder;

public class InventoryItemWithBuilderTest {
	public static void main(String[] args) {
		InventoryItemWithBuilder widget = InventoryItemWithBuilder.builder()
				.id(100)
				.qtyOnHand(250)
				.priceInCents(150)
				.restockTimeInDays(4)
				.bulkDiscountMinimum(25)
				.bulkDiscount(0.1d)
				.name("Widget")
				.shortDescription("a widget")
				.longDescription("This is a standard widget.")
				.build();
		
		System.out.println(widget);
		
		InventoryItemWithBuilder superWidget = widget.toBuilder()
				.id(101)
				.name("Super Widget")
				.shortDescription("a super widget")
				.longDescription("This is not your daddy's widget. It's super!")
				.build();
		
		System.out.println(superWidget);
		System.out.println("Equality test, expected false: " + superWidget.equals(widget));
		System.out.println("Hash code test, expected false: " + (superWidget.hashCode() == widget.hashCode()));
	}
}
