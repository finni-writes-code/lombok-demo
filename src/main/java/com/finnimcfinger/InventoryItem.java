package com.finnimcfinger;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class InventoryItem {
	private int id;
	private int qtyOnHand;
	private int priceInCents;
	private int restockTimeInDays;
	private int bulkDiscountMinimum;
	private double bulkDiscount;
	private String name; 
	private String shortDescription; 
	private String longDescription;
}
